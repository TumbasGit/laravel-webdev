<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="fn"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="ln"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender" value="Male">Male<br>
        <input type="radio" name="gender" value="Female">Female<br>
        <input type="radio" name="gender" value="Other">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Japanese">Japanese</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Chinese">Chinese</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="Language" value="Bahasa Indonesia">Bahasa Indonesia <br>
        <input type="checkbox" name="Language" value="English">English <br>
        <input type="checkbox" name="Language" value="Other">Other <br><br>
        <label>Bio:</label><br><br>
        <textarea cols="25" rows="10" name="bio"></textarea><br><br>
        <input type="submit" value="Kirim">
    </form>
</body>
</html>