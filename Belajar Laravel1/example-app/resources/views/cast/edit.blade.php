@extends('layout.master')
@section('title')    
Halaman Edit Cast
@endsection

@section('content')
<form method="POST" action='/cast/{{$cast->id}}'>
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Pemain</label>
      <input type="text" value='{{$cast->nama}}' name='nama' class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Umur Pemain</label>
      <input type="text" value='{{$cast->umur}}' name='umur' class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>Biodata Pemain</label>
        <textarea name="bio" class="form-control" cols="30" rows="5">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection