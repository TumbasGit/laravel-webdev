<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function menuAwal()
    {
        return view('page.index');
    }
}
