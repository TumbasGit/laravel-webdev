<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function autentikasi()
    {
        return view('page.form');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request["fn"];
        $namaBelakang = $request["ln"];
        $jenisKelamin = $request["gender"];
        $kebangsaan = $request["nationality"];
        $biodata = $request["bio"];

        return view('welcome', ["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);
    }
}
