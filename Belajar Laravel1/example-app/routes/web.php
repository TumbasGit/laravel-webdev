<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'menuAwal']);
Route::get('/register', [AuthController::class, 'autentikasi']);
Route::post('/welcome', [AuthController::class, 'kirim']);
Route::get('/data-tables', function(){
    return view('page.datatable');
});
Route::get('/table', function(){
    return view('page.table');
});

//CRUD

// Create Data
// Route pengarah form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
// Route pengarah menyimpan data inputan ke database table cast
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//Route menampilkan semua data di table cast database
Route::get('/cast', [CastController::class, 'index']);
//Route menampilkan detail data berdasar id
Route::get('/cast/{id}', [CastController::class, 'show']);

//Update Data
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//Route update cast berdasarkan id
Route::put('/cast/{id}', [CastController::class, 'update']);

//Delete Data
//Route untuk hapus data berdasarkan id
Route::delete('/cast/{id}', [CastController::class, 'destroy']);