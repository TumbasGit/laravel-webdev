<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");
echo "Name : " . $sheep->nama . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>";

$kodok = new frog("buduk");
echo "<br>Name : " . $kodok->nama . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump() . "<br>"; 

$sungokong = new ape("kera sakti");
echo "<br>Name : " . $sungokong->nama . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell() // "Auooo"

?>